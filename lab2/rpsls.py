def unit_tests():

    run_test("rock","rock","Draw")
    run_test("rock","scissors","Player 1")
    run_test("rock","paper","Player 2")
    run_test("rock","lizard","Player 1")
    run_test("rock","spock","Player 2")

    run_test("scissors","rock","Player 2")
    run_test("scissors","scissors","Draw")
    run_test("scissors","paper","Player 1")
    run_test("scissors","lizard","Player 1")
    run_test("scissors","spock","Player 2")
    
    run_test("paper","rock","Player 1")
    run_test("paper","scissors","Player 2")
    run_test("paper","paper","Draw")
    run_test("paper","lizard","Player 2")
    run_test("paper","spock","Player 1")

    run_test("lizard","rock","Player 2")
    run_test("lizard","scissors","Player 2")
    run_test("lizard","paper","Player 1")
    run_test("lizard","lizard","Draw")
    run_test("lizard","spock","Player 1")

    run_test("spock","rock","Player 1")
    run_test("spock","scissors","Player 1")
    run_test("spock","paper","Player 2")
    run_test("spock","lizard","Player 2")
    run_test("spock","spock","Draw")
    
def run_test(p1,p2,expected):
    print(p1, "vs. ",p2)
    result = win(p1,p2)
    try:
        assert result == expected
    except AssertionError:
            print("expected:", expected)
            print("result:", result)
    print("PASS")

def win(p1,p2):
    things = ("rock", "paper", "scissors", "lizard", "spock")
    if(p1 == things[0] and p2 == things[1]):
        return("Player 2")
    elif(p1 == things[0] and p2 == things[2]):
        return("Player 1")
    elif(p1 == things[0] and p2 == things[3]):
        return("Player 1")
    elif(p1 == things[0] and p2 == things[4]):
        return("Player 2")
    elif(p1 == things[1] and p2 == things[0]):
        return("Player 1")
    elif(p1 == things[1] and p2 == things[2]):
        return("Player 2")
    elif(p1 == things[1] and p2 == things[3]):
        return("Player 2")
    elif(p1 == things[1] and p2 == things[4]):
        return("Player 1")
    elif(p1 == things[2] and p2 == things[0]):
        return("Player 2")
    elif(p1 == things[2] and p2 == things[1]):
        return("Player 1")
    elif(p1 == things[2] and p2 == things[3]):
        return("Player 1")
    elif(p1 == things[2] and p2 == things[4]):
        return("Player 2")
    elif(p1 == things[3] and p2 == things[0]):
        return("Player 2")
    elif(p1 == things[3] and p2 == things[1]):
        return("Player 1")
    elif(p1 == things[3] and p2 == things[2]):
        return("Player 2")
    elif(p1 == things[3] and p2 == things[4]):
        return("Player 1")
    elif(p1 == things[4] and p2 == things[0]):
        return("Player 1")
    elif(p1 == things[4] and p2 == things[1]):
        return("Player 2")
    elif(p1 == things[4] and p2 == things[2]):
        return("Player 1")
    elif(p1 == things[4] and p2 == things[3]):
        return("Player 2")
    else:
        return("Draw")

def check_inputs(p1):
    things = ("rock", "paper", "scissors","lizard","spock")
    if p1 in things:
        return True
    else:
        return False

if __name__ == "__main__":
    #main()
    unit_tests()
