def win(p1, p2):
    things = ("rock", "paper", "scissors")
    if(p1 == things[0] and p2 == things[1]):
        return("Player 2")
    elif(p1 == things[0] and p2 == things[2]):
        return("Player 1")
    elif(p1 == things[1] and p2 == things[0]):
        return("Player 1")
    elif(p1 == things[1] and p2 == things[2]):
        return("Player 2")
    elif(p1 == things[2] and p2 == things[0]):
        return("Player 2")
    elif(p1 == things[2] and p2 == things[1]):
        return("Player 1")
    else:
        return("Draw")

    return None

def check_inputs(p1,p2):
    things = ("rock", "paper", "scissors")
    if p1 in things and p2 in things:
        return True
    else:
        return False

def run_test(p1,p2,expected):
    print(p1, "vs. ",p2)
    result = win(p1,p2)
    try:
        assert result == expected
    except AssertionError:
            print("expected:", expected)
            print("result:", result)
    print("PASS")
    
def unit_tests():
    run_test("rock","paper","Player 2")
    run_test("rock","scissors","Player 1")
    run_test("rock","rock","Draw")

def main():
    while(True):
        things = ("rock", "paper", "scissors")
        print("Choose from {},{},{}".format(things[0], things[1], things[2]))

        print("Player 1 input your choice:")
        p1 = input()
        print("Player 2 input your choice:")
        p2 = input()

        if(check_inputs(p1,p2)):
            result = win(p1, p2)
            print(result)
        else: 
            print("Input not correct.")
            continue

        cont = input("If you wish to continue type yes:")
        if cont != "yes":
            break


if __name__ == "__main__":
    #main()
    unit_tests()