from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.http.response import HttpResponseNotAllowed
from .models import Question


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def welcome(request):
    return HttpResponse("<h1>Welcome!</h1>")

def mamic(request):
    return HttpResponse("<h1>Mamić!</h1>")


from django.template import loader



def test(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request))

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

