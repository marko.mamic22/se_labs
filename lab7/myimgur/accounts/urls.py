from django.urls import path

from . import views

app_name = 'accounts'
urlpatterns = [
    path("sign-up", views.SignUpView.as_view(), name="signup"),
    path('<user_id>', views.user_profile, name='user'),
]
