from django.shortcuts import render, get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic

from app.models import Comment, Image


# Create your views here.

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

def user_profile(request,user_id):
    images = Image.objects.filter(user=user_id)
    comment = Comment.objects.filter(user = user_id)
    print(comment)
    votes = [ image.vote_by(request.user) for image in images ]
    context = {
        'images_with_votes': zip(images, votes),
        'comments': comment,
    }
    return render(request, 'app/user.html' ,context)
    