
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse,reverse_lazy
from .models import Image, Comment, Vote, Likes
from .forms import CommentForm, ImageForm

# Create your views here.


def index(request):
    images = Image.objects.order_by('-pub_date')
    votes = [ image.vote_by(request.user) for image in images ]
    context = {'images_with_votes': zip(images, votes)}
    return render(request, 'app/index.html', context)


def detail(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if(request.user.is_authenticated):
        like = Likes.objects.filter(user=request.user)
        context = {'image': image,
                'vote': image.vote_by(request.user),
               'comments': image.comment_set.filter(approved = True),    
               'form': CommentForm(),
               'likes': like,
               }

    context = {'image': image,
                'vote': image.vote_by(request.user),
               'comments': image.comment_set.filter(approved = True),    
               'form': CommentForm(),
               }
    if request.user.is_superuser:
        image = get_object_or_404(Image, pk=image_id)
        context = {'image': image,
                    'vote': image.vote_by(request.user),
                'comments': image.comment_set.all(),    
                'form': CommentForm(),
                'likes': like,
                }

    return render(request, 'app/detail.html', context)

def approve(request,image_id):
    test = request.POST.get("id")
    save_approved = Comment.objects.get( id = test)
    save_approved.approved = True
    save_approved.save()
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))


def create_image(request):
    if request.method == 'POST' and request.user.is_authenticated:
        form = ImageForm(request.POST)          
        if form.is_valid():
            saved_image = form.save(commit=False)
            saved_image.user = request.user
            saved_image = form.save()
            return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
    else:
        form = ImageForm()
    context = {
        'form': form,
     'action':'create'}
    return render(request, 'app/create_image.html', context)

def update_image(request,image_id):
    image = get_object_or_404(Image,pk=image_id)
    if request.method == 'POST' and request.user == image.user and request.user.is_authenticated or request.method == 'POST' and request.user.is_superuser:
        form = ImageForm(request.POST, instance=image)
        if form.is_valid():
            saved_image = form.save(commit=False)
            saved_image.user = request.user
            saved_image = form.save()
            return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
    else:
        form = ImageForm(instance=image)
    context = {'form': form,'action':'update'}
    return render(request, 'app/create_image.html', context)

def delete_image(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
    if request.method == 'POST' and request.user.is_authenticated and request.user == image.user or request.method == 'POST' and request.user.is_superuser:
        image.delete()
    return HttpResponseRedirect(reverse('app:index'))

def comment(request,image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        image = get_object_or_404(Image,pk=image_id)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.image = image
            comment.user = request.user
            comment.save()
            return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))
        else:
            return render(request,'app/detail.html',{
                'image':image,
                'comments':image.comment_set.all(),
                'form':form,
            })
    else:
        return HttpResponseRedirect(reverse('app:detail',args=(image_id,)))


def vote(request, image_id, upvote):
    image = get_object_or_404(Image, pk= image_id)
    vote = Vote.objects.filter(user=request.user, image=image).first()
    if vote:
        if vote.upvote == upvote:
            vote.delete()
            return None
        vote.upvote = upvote
    else:
        vote = Vote(user=request.user, image=image, upvote=upvote)
    try:
        vote.full_clean()
        vote.save()
    except:
        return None
    else:
        return vote

def setLike(request,comment_id, like):
    comment = Comment.objects.get( id = comment_id)
    like_obj = Likes.objects.filter(user=request.user, comment=comment_id).first()
    if like_obj:
        if like_obj.like == like:
            like_obj.delete()
            return None
        like_obj.like = like
    else:
        like_obj = Likes(user=request.user, comment=comment , like=like)
    try:
        like_obj.full_clean()
        like_obj.save()
    except:
        return None
    else:
        return like_obj

def like(request, image_id, comment_id):
    if request.method == 'POST' and request.user.is_authenticated:
        setLike(request,comment_id, True)
    return HttpResponseRedirect(reverse('app:detail',args=(image_id,)))

def upvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request,image_id, True)
    return HttpResponseRedirect(reverse('app:detail',args=(image_id,)))
def downvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request,image_id, False)
    return HttpResponseRedirect(reverse('app:detail',args=(image_id,)))