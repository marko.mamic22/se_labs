# Generated by Django 3.2.9 on 2021-12-23 00:41

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0009_auto_20211222_2319'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Like',
            new_name='Likes',
        ),
    ]
