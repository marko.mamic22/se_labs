# Git naredbe

test

## Useful to know

- git status (Trenutni status git repozitorija)
```
git status
```
- git add (Dodavanje datoteka u staged status spremnih za commitanje)
```
git add novifile.py
```
- git log (Popis prijašnjih commitova)
```
git log
```
- git commit (Lokalno spremanje izmjena spremnih za pushanje na remote)
```
git commit -m "Poruka commita"
```
- git diff (Razlika u fileovima pri izmjenama)
```
git diff
```
- git push (Stavljanje commitova na remote)
```
git push origin master
```
- git config (Konfiguracija git enivromenta)
```
git config user.name "Marko Mamić"
```
test v2
git pull (Povlači promjene s repozitorija)
```
git pull origin master
```
